# dualscyther's layout

-   Map fn (layer 1) to the left control key
-   Map grave to the esc key (top left corner)
-   caps lock key acts as ctrl when held, and esc when tapped
-   Map esc to the grave key (top right corner)
